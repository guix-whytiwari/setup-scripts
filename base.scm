(use-modules (gnu) (gnu system nss) (guix utils))
(use-service-modules networking)
(use-package-modules certs)

(operating-system
  (host-name "HOSTNAME")
  (timezone "Asia/Kolkata")
  (locale "en_US.utf8")

  (keyboard-layout (keyboard-layout "us"))

  (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader)
                (targets '("/boot/efi"))
                (keyboard-layout keyboard-layout)))

  (mapped-devices
   (list (mapped-device
          (source (uuid "LUKS_UUID"))
          (target "guix-crypt")
          (type luks-device-mapping))))

  (file-systems (append
                 (list (file-system
                         (device (file-system-label "guix-root"))
                         (mount-point "/")
                         (type "btrfs")
                         (options "subvol=root,compress=zstd:3")
                         (dependencies mapped-devices))
                       (file-system
                         (device (file-system-label "guix-root"))
                         (mount-point "/boot")
                         (type "btrfs")
                         (options "subvol=boot,compress=zstd:3")
                         (dependencies mapped-devices))
                       (file-system
                         (device (file-system-label "guix-root"))
                         (mount-point "/data")
                         (type "btrfs")
                         (options "subvol=data,compress=zstd:3")
                         (dependencies mapped-devices))
                       (file-system
                         (device (file-system-label "guix-root"))
                         (mount-point "/gnu")
                         (type "btrfs")
                         (options "subvol=gnu,compress=zstd:3")
                         (dependencies mapped-devices))
                       (file-system
                         (device (file-system-label "guix-root"))
                         (mount-point "/home")
                         (type "btrfs")
                         (options "subvol=home,compress=zstd:3")
                         (dependencies mapped-devices))
                       (file-system
                         (device (file-system-label "guix-root"))
                         (mount-point "/var/log")
                         (type "btrfs")
                         (options "subvol=log,compress=zstd:3")
                         (dependencies mapped-devices))
                       (file-system
                         (device (file-system-label "guix-root"))
                         (mount-point "/swap")
                         (type "btrfs")
                         (options "subvol=swap")
                         (dependencies mapped-devices))
                       (file-system
                         (device (uuid "EFI_UUID" 'fat))
                         (mount-point "/boot/efi")
                         (type "vfat")))
                 %base-file-systems))

  ;; Specify a swap file for the system, which resides on the
  ;; root file system.
  ;; (swap-devices (list (swap-space
  ;;                      (target "/swapfile"))))

  ;; Create user `bob' with `alice' as its initial password.
  (users (cons (user-account
                (name "whytiwari")
                (comment "Yash Tiwari")
                (group "users")
                (supplementary-groups '("wheel" "netdev"
                                        "audio" "video")))
               %base-user-accounts))

  (packages (append (list
                     ;; for HTTPS access
                     nss-certs
                     )
                    %base-packages))

  (services (append (list (service network-manager-service-type)
                          (service wpa-supplicant-service-type))
                    %base-services))

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss))
