# echo $1
# echo $2

if [[ $1 = "--help" ]]; then
    echo "Help for guix-btrfs installation"
    echo "arg 1 -> phase [1, 2, 3]"
    echo "arg 2 -> partition [/dev/nvme0n1]"
    echo "arg 3 -> hostname [deck/strix] [3]"
fi

if [[ $1 = "1" ]]; then
    echo "Executing phase 1"
    echo "Formatting block device [$2]"
    sudo nvme format $2 -s 2
    sudo blkdiscard $2
fi

if [[ $1 = "2" ]]; then
    echo "Executing phase 2"
    echo "Partitioning [$2]"
    parted $2 -- mklabel gpt
    parted $2 -- mkpart ESP fat32 1MiB 513MiB
    parted $2 -- set 1 boot on
    mkfs.vfat -n guix-efi "$2"p1
    parted $2 -- mkpart primary 513MiB 100%
    cryptsetup luksFormat "$2"p2
    cryptsetup open "$2"p2 guix-crypt
    mkfs.btrfs -L guix-root /dev/mapper/guix-crypt
    echo "Creating btrfs subvolumes for [$2]"
    mount -t btrfs /dev/mapper/guix-crypt /mnt
    btrfs subvolume create /mnt/root
    btrfs subvolume create /mnt/boot
    btrfs subvolume create /mnt/data
    btrfs subvolume create /mnt/gnu
    btrfs subvolume create /mnt/home
    btrfs subvolume create /mnt/log
    btrfs subvolume create /mnt/swap
    echo "Creating btrfs snapshots for root & home subvolumes"
    btrfs subvolume snapshot -r /mnt/root /mnt/root-blank
    btrfs subvolume snapshot -r /mnt/home /mnt/home-blank
    umount /mnt
    echo "btrfs configuration complete"
    echo "Mounting partitions for final setup"
    mount -o subvol=root /dev/mapper/guix-crypt /mnt
    cd /mnt
    mkdir -p boot etc data gnu home var/log swap
    echo "Mounting btrfs subvolumes"
    mount -o subvol=boot /dev/mapper/guix-crypt boot
    mount -o subvol=data /dev/mapper/guix-crypt data
    mount -o subvol=gnu /dev/mapper/guix-crypt gnu
    mount -o subvol=home /dev/mapper/guix-crypt home
    mount -o subvol=log /dev/mapper/guix-crypt var/log
    mount -o subvol=swap /dev/mapper/guix-crypt swap
    echo "Mounting efi partition"
    mkdir -p boot/efi
    mount $2p1 boot/efi
    herd start cow-store /mnt
fi

if [[ $1 = "3" ]]; then
    echo "Executing phase 3"
    guix_hostname=$3
    guix_efi=$(blkid -s UUID -o value $2p1)
    guix_luks=$(blkid -s UUID -o value $2p2)
    cat base.scm | sed 's/HOSTNAME/'${guix_hostname}'/' \
		 | sed 's/EFI_UUID/'${guix_efi}'/' \
	         | sed 's/LUKS_UUID/'${guix_luks}'/' \
		 | tee /mnt/etc/config.scm
    echo Execute the following command -
    echo guix system init /mnt/etc/config.scm /mnt
fi
